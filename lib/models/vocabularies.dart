class Vocabulary {
  final String word;
  final String partOfSpeech;
  final String pronunciation;
  final String translation;
  final Definition definition;
  final List<Definition> examples;

  Vocabulary({
    required this.word,
    required this.partOfSpeech,
    required this.pronunciation,
    required this.translation,
    required this.definition,
    required this.examples,
  });

  Vocabulary copyWith({
    String? word,
    String? partOfSpeech,
    String? pronunciation,
    String? translation,
    Definition? definition,
    List<Definition>? examples,
  }) =>
      Vocabulary(
        word: word ?? this.word,
        partOfSpeech: partOfSpeech ?? this.partOfSpeech,
        pronunciation: pronunciation ?? this.pronunciation,
        translation: translation ?? this.translation,
        definition: definition ?? this.definition,
        examples: examples ?? this.examples,
      );

  factory Vocabulary.fromJson(Map<String, dynamic> json) => Vocabulary(
    word: json["word"],
    partOfSpeech: json["part_of_speech"],
    pronunciation: json["pronunciation"],
    translation: json["translation"],
    definition: Definition.fromJson(json["definition"]),
    examples: List<Definition>.from(json["examples"].map((x) => Definition.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "word": word,
    "part_of_speech": partOfSpeech,
    "pronunciation": pronunciation,
    "translation": translation,
    "definition": definition.toJson(),
    "examples": List<dynamic>.from(examples.map((x) => x.toJson())),
  };
}

class Definition {
  final String english;
  final String vietnamese;

  Definition({
    required this.english,
    required this.vietnamese,
  });

  Definition copyWith({
    String? english,
    String? vietnamese,
  }) =>
      Definition(
        english: english ?? this.english,
        vietnamese: vietnamese ?? this.vietnamese,
      );

  factory Definition.fromJson(Map<String, dynamic> json) => Definition(
    english: json["english"],
    vietnamese: json["vietnamese"],
  );

  Map<String, dynamic> toJson() => {
    "english": english,
    "vietnamese": vietnamese,
  };
}
