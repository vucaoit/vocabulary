import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vocabulary_training/models/vocabularies.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  Future<List<Vocabulary>> getVocabularies() async {
    final String response = await rootBundle.loadString('assets/vocabularies.json');
    final data = await jsonDecode(response) as List<dynamic>;
    final listVocabularies = data.map((e) => Vocabulary.fromJson(e)).toList();
    return listVocabularies;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home"),
      ),
      body: SafeArea(
        child: FutureBuilder(
          future: getVocabularies(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            } else {
              if (snapshot.hasError) {
                return ErrorWidget(snapshot.error ?? 'error');
              } else {
                final data = snapshot.data ?? [];
                return ListView.builder(
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    final item = data.elementAt(index);
                    return Card(
                      child: ListTile(
                        title: Text(item.word),
                        subtitle: Text(item.translation),
                      ),
                    );
                  },
                );
              }
            }
          },
        ),
      ),
    );
  }
}
