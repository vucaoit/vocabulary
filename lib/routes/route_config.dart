import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vocabulary_training/routes/routes_name.dart';
import 'package:vocabulary_training/screens/home_screen/home_screen.dart';

final routeConfig = GoRouter(initialLocation: "/", routes: [
  _routeBuilder(route: Routing.home, builder: (context, state) => const HomeScreen()),
]);

_routeBuilder(
    {required RouteModel route,
    required Widget Function(BuildContext, GoRouterState)? builder}) {
  return GoRoute(name: route.name, path: route.path, builder: builder);
}
