class RouteModel{
  final String name;
  final String path;

  RouteModel({required this.name, required this.path});
}

class Routing{
  const Routing._();
  static final home = RouteModel(name: "home", path: "/");
}